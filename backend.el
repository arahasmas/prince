;;;;;;;
;;; Function definitions, keybindings and
;;; other editor settings for PRINCE
;;;

(defun pr--narrow-buffer (section)
  "Narrow the current buffer to show only the selected SECTION.
Accept in input the name of a tag ('header', 'body' or 'footer').
If the string is empty the default tag is 'BODY'."
  (interactive
   "sEnter the tag to which narrow the buffer:")
  (save-excursion
    (if (string= section "")
	(setq tag "BODY")
      (setq tag (upcase section)))
    (if (not (member tag '("DOCUMENT" "HEADER" "BODY" "FOOTER")))
	(user-error "ERROR: Possible tags are '%s', '%s', '%s' or '%s'."
		    "document" "header" "body" "footer"))
    (goto-char (point-min))
    (let ((beg (progn
		 (search-forward-regexp (concat "^@" tag "$"))
		 (line-beginning-position)))
	  (end (progn
		 (search-forward-regexp (concat "^@/" tag "$"))
		 (line-end-position))))
      (narrow-to-region beg end))))

;;
;; The backend to convert the org source to html
;;

(defun pr--select-section-region (sect)
  "Select the region enclosed in the SECT tags."
  (setq sect (upcase sect))
  (goto-char (point-min))
  (let ((beg (progn
							 (search-forward-regexp (concat "^"
																							(if (string-prefix-p "@" sect) "" "@")
																							sect "$"))
							 (forward-line)
							 (beginning-of-line)
							 (point)))
				(end (progn
							 (search-forward-regexp (concat "^"
																							(if (string-prefix-p "@" sect) "" "@")
																							"/" sect "$"))
							 (forward-line -1)
							 (end-of-line)
							 (point))))
    (set-mark beg)
    (goto-char end)
    (activate-mark)))

(defun pr--unfill-region ()
  "Unfill region at point.
Works only in org-mode"
  (let ((fill-column (point-max)))
    (if (and transient-mark-mode mark-active)
	(org-fill-paragraph nil t))))

(defun pr--untabify ()
	"Transform all initial tabs in spaces."
	(let ((tab-width 2))
		(untabify (point-min) (point-max))))

(defun pr--narrow-to-section-buffer (buf sect)
  "Narrow into the buffer BUF to the region enclosed in the SECT tags."
	(interactive)
  (setq sect (upcase sect))
  (with-current-buffer buf
    (goto-char (point-min))
    (let ((beg (progn
		 (search-forward-regexp (concat "^"
						(if (string-prefix-p "@" sect) "" "@")
						sect "$"))
		 (line-beginning-position)))
	  (end (progn
		 (search-forward-regexp (concat "^"
						(if (string-prefix-p "@" sect) "" "@")
						"/" sect "$"))
		 (line-end-position))))
      (narrow-to-region beg end))))

(defun pr--substitute-into (buf target subst)
  "Replace into the buffer BUF every TARGET string with SUBST."
  (with-current-buffer buf
    (goto-char (point-min))
    (while (search-forward target nil t)
      (replace-match subst))))

(defun pr--re-substitute-into (buf target subst)
  "Replace into the buffer BUF every regex TARGET string with SUBST."
  (with-current-buffer buf
    (goto-char (point-min))
    (while (re-search-forward target nil t)
      (replace-match subst))))

(defun pr--current-date-time ()
  "Return a formatted string with current date and time."
  (format-time-string "%Y-%m-%dT%H:%M:%S"))

(defun pr--add-costant-header-to (buf)
  "Insert into the buffer BUF an html header."
  (let ((html-head (concat
		    "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
		    "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\"\n"
		    "\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n"
		    "<html xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"en\" xml:lang=\"en\">\n\n"
		    "<head>\n"
		    "<meta http-equiv=\"Content-Type\" content=\"text/html;charset=utf-8\" />\n"
		    "<title>PRINCE Is Not a Cheatsheet for Emacs</title>\n"
		    "<meta name=\"generator\" content=\"Prince Backend\" />\n"
		    "<meta name=\"date\" content=\"" (pr--current-date-time) "\" />\n"
		    "<meta name=\"description\" content=\"A mind map for the Emacs commands.\" />\n"
		    "<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />\n"
		    "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\" />\n"
		    "<link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\" />\n"
		    "</head>\n")))
	(with-current-buffer buf
	  (goto-char (point-min))
	  (insert html-head))))

(defun pr--add-costant-footer-to (buf)
  "Insert into the buffer BUF an html footer."
  (let ((html-foot "\n</html>"))
    (with-current-buffer buf
      (goto-char (point-max))
      (insert html-foot))))

(defun pr--wide-buffer (buf)
  "Remove restrictions from the buffer BUF."
  (with-current-buffer buf
    (widen)))

(defun pr--add-to-buffer (buf str)
  "Add the string STR to the buffer BUF."
  (with-current-buffer buf
    (goto-char (point-max))
    (insert (concat "\n" str))))

(defun pr--translate-display-markup (str)
  "Translate the string STR from Prince-markup to html."
  (cond ((string-prefix-p "#" str t)
	 nil)
	((string-prefix-p "@C" str t)
	 nil)
	((string-prefix-p "@nl" str t)
	 (replace-regexp-in-string "@nl" "<br/>" str t))
	((string-prefix-p "@DOCUMENT" str t)
	 (replace-regexp-in-string "@DOCUMENT" "<body>" str t))
	((string-prefix-p "@/DOCUMENT" str t)
	 (replace-regexp-in-string "@/DOCUMENT" "</body>" str t))
	((string-prefix-p "@HEADER" str t)
	 (replace-regexp-in-string "@HEADER" "<div class=\"head\">" str t))
	((string-prefix-p "@/HEADER" str t)
	 (replace-regexp-in-string "@/HEADER" "</div>" str t))
	((string-prefix-p "@BODY" str t)
	 (replace-regexp-in-string "@BODY" "<div class=\"body\">" str t))
	((string-prefix-p "@/BODY" str t)
	 (replace-regexp-in-string "@/BODY" "</div>" str t))
	((string-prefix-p "@FOOTER" str t)
	 (replace-regexp-in-string "@FOOTER" "<div class=\"footer\">" str t))
	((string-prefix-p "@/FOOTER" str t)
	 (replace-regexp-in-string "@/FOOTER" "</div>" str t))
	((string-prefix-p "@title{" str t)
	 (replace-regexp-in-string "@title{\\(.*?\\)}"
				   "<div class=\"title\">\\1</div>" str t))
	((string-prefix-p "@subtitle{" str t)
	 (replace-regexp-in-string "@subtitle{\\(.*?\\)}"
				   "<div class=\"subtitle\">\\1</div>" str t))
	((string-prefix-p "@subsubtitle{" str t)
	 (replace-regexp-in-string "@subsubtitle{\\(.*?\\)}"
				   "<div class=\"subsubtitle\">\\1</div>" str t))
	
	((string-prefix-p "    - @subsection{" str t)
	 (replace-regexp-in-string "^ +- @subsection{\\(.*?\\)}"
				   "<div class=\"subsection\">\\1</div>" str t))
	
	((string-prefix-p "- " str t)
	 (replace-regexp-in-string "^- \\(.*?\\)$"
				   "<div class=\"item-level-1\">\\1</div>" str t))
	((string-prefix-p "  - " str t)
	 (replace-regexp-in-string "^  - \\(.*?\\)$"
				   "<div class=\"item-level-2\">\\1</div>" str t))
	((string-prefix-p "    - " str t)
	 (replace-regexp-in-string "^    - \\(.*?\\)$"
				   "<div class=\"item-level-3\">\\1</div>" str t))
	((string-prefix-p "      - " str t)
	 (replace-regexp-in-string "^      - \\(.*?\\)$"
				   "<div class=\"item-level-4\">\\1</div>" str t))
	((string-prefix-p "        - " str t)
	 (replace-regexp-in-string "^        - \\(.*?\\)$"
				   "<div class=\"item-level-5\">\\1</div>" str t))
	((string-prefix-p "@p" str t)
	 (replace-regexp-in-string "^@p \\(.*?\\)$"
				   "<p>\\1</p>" str t))
	(t nil)))
  
(defun pr--filter-source-to-dest (source-buf dest-buf)
  (with-current-buffer source-buf
    (goto-char (point-min))
    (while (not (eobp))
      (let* ((lb (line-beginning-position))
	     (le (line-end-position))
	     (line (buffer-substring-no-properties lb le)))
	(let ((translated-line (pr--translate-display-markup line)))
	  (if (stringp translated-line)
	      (pr--add-to-buffer dest-buf translated-line)))
	(forward-line 1)))))

(defun pr--translate-inline-markup (buf)
  ;; @nl
  (pr--substitute-into buf
											 "@nl"
											 "<br/>")
  ;; &gt; &lt;
  (pr--substitute-into buf
											 "&gt;"
											 ">")
  (pr--substitute-into buf
											 "&lt;"
											 "<")
  ;; @italic{}
  (pr--re-substitute-into buf
													"@italic{\\(.*?\\)}"
													"<i>\\1</i>")
  ;; @bold{}
  (pr--re-substitute-into buf
													"@bold{\\(.*?\\)}"
													"<b>\\1</b>")
  ;; @roman{}
  (pr--re-substitute-into buf
													"@roman{\\(.*?\\)}"
													"<span class=\"roman\">\\1</span>")
  ;; @teletype{}
  (pr--re-substitute-into buf
													"@teletype{\\(.*?\\)}"
													"<span class=\"tt\">\\1</span>")
  ;; @keybinding{}
  (pr--re-substitute-into buf
													"@keybinding{\\(.*?\\)}"
													"<span class=\"kbd\">\\1</span>")
  ;; @command{}
  (pr--re-substitute-into buf
													"@command{\\(.*?\\)}"
													"<span class=\"cmd\">\\1</span>")
  ;; @link{}
  (pr--re-substitute-into buf
													"@link{\\(.*?\\)}"
													"<a href=\"\\1\">\\1</a>")
  ;; @extlink{}{}
  (pr--re-substitute-into buf
													"@extlink{\\(.*?\\)}[[:blank:]\n]*{\\(.*?\\)}"
													"<a class=\"extlink\" href=\"\\2\" target=\"_blank\">\\1</a>")
	;; @variable{}{}
  (pr--re-substitute-into buf
													"@variable{\\(.*?\\)}[[:blank:]\n]*{\\(.*?\\)}"
													"<span class=\"variable\"><a class=\"extlink\" href=\"\\2\" target=\"_blank\">\\1</a></span>")
	;; @varwl{}
  (pr--re-substitute-into buf
													"@varwl{\\(.*?\\)}"
													"<span class=\"varwl\">\\1</a></span>")
  ;; @function{}{}
  (pr--re-substitute-into buf
													"@function{\\(.*?\\)}[[:blank:]\n]*{\\(.*?\\)}"
													"<span class=\"function\"><a class=\"extlink\" href=\"\\2\" target=\"_blank\">\\1</a></span>")
  )

(defun pr--export-to-html ()
  "Export the source file of Prince to html."
  (save-excursion
    ;; Si stabiliscono i buffer di lavoro: quello sorgente e quello di destinazione
    (let ((source-buffer (generate-new-buffer (generate-new-buffer-name "source")))
	  (dest-buffer (generate-new-buffer (generate-new-buffer-name "destination"))))
      ;; si copia il buffer corrente su quello sorgente
      (let ((old-buf (current-buffer)))
	   (with-current-buffer source-buffer
	     (insert-buffer-substring-no-properties old-buf)))
      ;; si aggiungono le prime sezioni fisse al file destinazione
      (pr--add-costant-header-to dest-buffer)
			;; poichè le regex richiedono solo spazi, si trasformano tutti i
      ;; <tab> iniziali in spazi
			(with-current-buffer source-buffer
				(pr--untabify))
			;; all'interno della sezione @BODY si eliminano tutti gli accapo inutili
      (with-current-buffer source-buffer
				(pr--select-section-region "BODY")
				(pr--unfill-region))
      ;; si restringe il buffer sorgente alla sola sezione @DOCUMENT e si
      ;; traduce il markup inline nel corrispettivo html
      (pr--narrow-to-section-buffer source-buffer "DOCUMENT")
      (pr--translate-inline-markup source-buffer)
      ;; si analizza il file sorgente riga per riga
      ;; ogni riga viene tradotta da Prince-markup a html
      ;; e scritta nel file di destinazione
      (pr--filter-source-to-dest source-buffer dest-buffer)
      ;; scrive il footer nel file di destinazione
      (pr--add-costant-footer-to dest-buffer)
      ;; si salva il file destinazione come "../public/index.html" e
      ;; si chiude il buffer.
      (with-current-buffer dest-buffer
	(write-file "./public/index.html" nil)
	(kill-buffer))
      ;; si chiude anche il file sorgente temporaneo
      (with-current-buffer source-buffer
	(kill-buffer)))))


;;; The main function

(defun pr--publish ()
  "Create the html file, trace the time and manages messages to the user."
  (interactive)
  (let ((time (current-time)))
    (message "Exporting to html...")
    (pr--export-to-html)
    (message "Exported to html in %.06f secs." (float-time (time-since time)))
    ))
