# Prince

*Prince Is Not a Cheatsheet for Emacs*

------------------------------------------------------------------------

<div align="left">

[![version](https://img.shields.io/badge/version-0.17-orange?style=plastic)](https://arahasmas.gitlab.io/prince/) 
[![pipeline status](https://img.shields.io/gitlab/pipeline/arahasmas/prince?color=brightgreen&style=plastic&logo=gitlab)](https://gitlab.com/arahasmas/prince/commits/master) 
[![licence](https://img.shields.io/badge/licence-GNU%20GPL%20v3-blue?style=plastic&logo=gnu)](https://www.gnu.org/licenses/gpl-3.0.html) 
[![issues](https://img.shields.io/badge/issues-welcome-ff69b4?style=plastic&logo=gitlab)](https://gitlab.com/arahasmas/prince/issues/new)

</div>

Prince is a document freely available on the web halfway between the
mind map and the [cheat
sheet](https://www.emacswiki.org/emacs/CategoryReferenceSheet).

It contains a list of the [Emacs](https://www.gnu.org/software/emacs/)
commands, the keybindings, the variables, the functions, the hooks.

The project is still under development, but a first version is already
available as a [gitlab page](https://arahasmas.gitlab.io/prince).


## Usage

The main document is available from the link above; from there you can
get a paper version of the document with the browser's "*print*"
function. To obtain the best layout, I recommend using the "landscape"
page format and adjusting the zoom factor wisely.


## Beware of the browser

This document was created, tested and optimized on Firefox. I don't know
*if* nor *how* it is rendered on other browsers.


## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.
